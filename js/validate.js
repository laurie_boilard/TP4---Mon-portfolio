let error = [];
const nom = document.getElementById("nom");
const prenom = document.getElementById("prenom");
const email = document.getElementById("courriel");

const sujet = document.getElementById('sujet')
const demande = document.getElementById("commentaire");

const captcha = document.getElementById('captcha')

const formulaire = document.getElementById('form')
formulaire.addEventListener('submit', validerChamps)

// Outils de comparaison
const emailTest = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
const phoneTest = /\+?([\d|\(][\h|\(\d{3}\)|\.|\-|\d]{4,}\d)/

function validerChamps(e){
  const messages = []

  // Pour savoir s'il y a un caractères quelconque dans les input
  if (nom.value.length <= 0) {
      messages.push('Le champs nom doit contenir des caractères')
  }

  if (prenom.value.length <= 0) {
    messages.push('Le champs prénom doit contenir des caractères')
  }

  if (courriel.value.length <= 0) {
    messages.push('Le champs courriel doit contenir des caractères')
  }

  if (telephone.value.length <= 0) {
    messages.push('Le champs téléphone doit contenir des caractères')
  }

  if (sujet.value.length <= 0) {
    messages.push('Le titre du sujet doit contenir des caractères')
  }

  if (demande.value.length <= 0) {
    messages.push('Le champs de votre demande doit contenir des caractères')
  }

  if(emailTest.test(email.value) === false){
    messages.push("Le courriel n'est pas valide")
  }

  if(phoneTest.test(telephone.value) === false){
    messages.push("Le téléphone n'est pas valide")
  }

  if (messages.length > 0) {
      e.preventDefault()
      const ul = document.getElementById('messages')
      ul.innerHTML = ""
      for(let i = 0; i < messages.length; i++){
          const li = document.createElement('li')
          const texte = document.createTextNode(messages[i])
          li.appendChild(texte)
          ul.appendChild(li)
          li.classList.add('red')
      }
      
  }  
}












