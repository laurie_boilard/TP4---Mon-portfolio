TP4 - Mon portfolio

Trois animations par transition:

1- Animation sur le menu principal (qui se retrouve sur toutes les pages),
et sur le menu secondaire (qui se retrouve aux pages partenariats et blog)
   Dans le CSS: à partir de la ligne 101

2- Animation sur le scroll des pages H2
   Dans toutes les pages où sont mes H2
   Dans le CSS : ligne 610

3- Animation d'un élément SVG
   À la page contact.html sur l'icône du téléphone
   Dans le CSS : ligne 587
   

Animation par Keyframe
1- Sur la page index.html dans le haut
   Dans le CSS : ligne 632

Animation contrôlée par JavaScript:

1- Sur la page index.html, vous cliquez sur cliquez ici et l'infolettre s'affiche
   Dans le CSS : à partir de la ligne 509
